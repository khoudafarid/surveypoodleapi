// Load env vars
if (process.env.ENVIRONMENT != 'HEROKU') {
    require('dotenv').config();
}
// Imports
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const { PORT = 3000 } = process.env;
const { db } = require('./config/db.config');

// Initialize
const app = express();
// Middleware
app.use(cookieParser())
app.use(cors());
app.use(express.json());
app.use(require('./middleware/auth.middleware'));

db.authenticate().then(r => {
    app.get('/', (rq, rs) => rs.status(200).send('Welcome to the Course Feedback API.'));

    // Routes
    app.use('/v1/api/users', require('./routes/user.route'));
    app.use('/v1/api/surveys', require('./routes/survey.route'));
    app.use('/v1/api/questions', require('./routes/question.route'));

    app.listen(PORT, () => console.log(`Server started on port ${PORT}...`));
}).catch(e => {
    console.log(e);
});
