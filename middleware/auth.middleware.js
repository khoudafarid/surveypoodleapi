const { UserSession } = require('../models/models');
const TokenTool = require('../models/token.model');
const Response = require('../models/response.model');
const publicPaths = [
    '/',
    '/v1/api/users/register',
    '/v1/api/users/login'
];

/**
 * Authentication middleware
 * Verify the existence of JWT
 * Checks JWT validity
 */
module.exports = async (req, res, next) => {

    if (publicPaths.includes(req.path)) { return next(); }

    const { authorization } = req.headers;
    const r = new Response();

    try {

        if (authorization == undefined || authorization.indexOf('Bearer ') < 0) {
            throw ({ status: 401, message: 'No or invalid authorization header present' });
        }

        const token = authorization.split('Bearer ')[1];

        if (!token) {
            throw ({ status: 401, message: 'No valid token was found' });
        }

        const validToken = TokenTool.verify(token);

        if (!validToken) {
            throw({ status: 401, message: 'The token you provided is not valid' });
        }

        const { payload } = TokenTool.decode(token);   

        const session = await UserSession.findOne({
            where: {
                user_id: payload.user_id,
                active: 1
            }
        });

        if (session == null) {
            throw ({ status: 401, message: 'User is no longer active' });
        }

    } catch (e) {
        r.setError(e);
        return res.status(r.status).json(r);
    }

    return next();

};