const jose = require('jose');
const { JWT, JWK } = jose;

const KEY = JWK.asKey({
    kty: 'oct',
    k: process.env.HASH_SECRET_TOKEN
});

const AUDIENCE = 'urn:survey-poodle:client';
const ISSUER = 'https://survey-poodle.herokuapp.com/'

class TokenTool {

    static sign(payload) {
        return JWT.sign(payload, KEY, {
            audience: AUDIENCE,
            issuer: ISSUER,
            header: {
                typ: 'JWT'
            }
        });
    }

    static verify(token) {
        return JWT.verify(token, KEY, {
            audience: AUDIENCE,
            issuer: ISSUER
        });
    }

    static decode(token) {
        return JWT.decode(token, {
            complete: true
        });
    }
}

module.exports = TokenTool;