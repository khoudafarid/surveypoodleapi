const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');

class Question extends Model {}

Question.init({
    question_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    label: DataTypes.STRING,
    active: DataTypes.TINYINT
}, {
    sequelize: db,
    modelName: 'question'
});

module.exports = Question;