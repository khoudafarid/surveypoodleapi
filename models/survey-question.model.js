const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');

class SurveyQuestion extends Model {}

SurveyQuestion.init({
    survey_question_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    survey_id: DataTypes.INTEGER,
    question_id: DataTypes.INTEGER
}, {
    sequelize: db,
    modelName: 'survey_question',
    tableName: 'survey_question'
});

module.exports = SurveyQuestion;
