const User = require('./user.model');
const Survey = require('./survey.model');
const Question = require('./question.model');
const SurveyQuestion = require('./survey-question.model');
const UserSession = require('./user-session.model');

Question.belongsToMany(Survey, {
    through: SurveyQuestion,
    foreignKey: 'question_id'
});

Survey.belongsToMany(Question, {
    through: SurveyQuestion,
    foreignKey: 'survey_id'
});

Survey.belongsTo(User, {
    foreignKey: 'user_id'
})

SurveyQuestion.hasMany(Survey, {
    foreignKey: 'survey_id'
});

SurveyQuestion.hasMany(Question, {
    foreignKey: 'question_id'
});

User.belongsTo(UserSession, {
    foreignKey: 'user_id'
});

module.exports = {
    User,
    Survey, 
    Question,
    SurveyQuestion,
    UserSession
};