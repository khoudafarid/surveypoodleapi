const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');

class Survey extends Model {}

Survey.init({
    survey_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    active: DataTypes.TINYINT,
    user_id: DataTypes.INTEGER
}, {
    sequelize: db,
    modelName: 'survey'
});

module.exports = Survey;