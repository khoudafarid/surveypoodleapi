const { Model, DataTypes } = require('sequelize');
const { db } = require('../config/db.config');
const bcrypt = require('bcrypt');

class User extends Model {

    static generatePassword(plainPassword) {
        return bcrypt.hashSync(plainPassword, parseInt(process.env.HASH_ROUNDS));
    }

    static comparePassword(plainPassword, hashPassword) {
        return bcrypt.compareSync(plainPassword, hashPassword);
    }
}

User.init({
    user_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    active: DataTypes.TINYINT
}, {
    sequelize: db,
    modelName: 'user'
});

module.exports = User;