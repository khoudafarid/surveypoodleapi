const { Survey, User, Question, SurveyQuestion } = require('../models/models');
const router = require('express').Router();
const moment = require('moment');
const Response = require('../models/response.model');

router.get('/', async (req, res) => {

    const r = new Response();

    try {
        r.data = await Survey.findAll({
            where: {
                active: 1
            },
            attributes: ['survey_id', 'title', 'description', 'created_at', 'updated_at'],
            include: [
                {
                    model: User,
                    attributes: ['user_id', 'username', 'created_at', 'updated_at']
                }
            ]
        });
    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.get('/:survey_id/questions', async (req, res) => {

    const r = new Response();
    const { survey_id } = req.params;

    try {

        if (!survey_id) {
            throw ({ status: 400, message: 'No survey id was provided' });
        }

        r.data = await Survey.findOne({
            where: {
                survey_id: survey_id,
                active: 1
            },
            include: [{
                model: User,
                as: 'user',
                attributes: ['user_id', 'username', 'created_at', 'updated_at']
            }, {
                model: Question,
                as: 'questions',
                attributes: ['question_id', 'label', 'created_at', 'updated_at'],
                // Don't get linking table results
                through: { attributes: [] }
            }],
        });

    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.get('/:survey_id', async (req, res) => {

    const r = new Response();
    const { survey_id } = req.params;

    try {

        if (!survey_id) {
            throw ({ status: 400, message: 'No survey id was provided' });
        }

        r.data = await Survey.findOne({
            where: {
                survey_id: survey_id,
                active: 1
            },
            attributes: ['survey_id', 'title', 'description', 'created_at', 'updated_at'],
            include: [{
                model: User,
                as: 'user',
                attributes: ['user_id', 'username', 'created_at']
            }]
        });

    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/', async (req, res) => {

    const r = new Response();
    const { survey } = req.body;

    try {

        if (!survey) {
            throw ({ status: 400, message: 'No survey information was provided.' });
        }

        r.data = await Survey.create(survey);
        r.status = 201;

    } catch (e) {
        let error = e;
        if (e.errors.length > 0 && e.errors[0].message) {
            error = {
                message: e.errors[0].message,
                status: 400
            }
        }
        r.setError(error);
    }
    return res.status(r.status).json(r);
});

module.exports = router;
