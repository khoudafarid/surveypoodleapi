const { Question, Survey, SurveyQuestion } = require('../models/models');
const router = require('express').Router();
const Response = require('../models/response.model');

router.get('/', async (req, res) => {
    const r = new Response();
    try {
        r.data = await Question.findAll({
            attributes: ['question_id', 'label', 'created_at', 'updated_at'],
            where: {
                active: 1
            }
        });
    } catch (e) {
        r.setError(e);
    }
    return res.status(r.status).json(r);
});

router.get('/:question_id', async (req, res) => {

    const r = new Response();
    const { question_id } = req.params;

    try {

        if (!question_id) {
            throw ({ status: 400, message: 'No question id was provided' });
        }

        r.data = await Question.findOne({
            attributes: ['question_id', 'label', 'created_at', 'updated_at'],
            where: {
                question_id: question_id,
                active: 1
            }
        });
    } catch (e) {
        r.setError(e);
    }

    return res.status(r.status).json(r);

});

router.post('/', async (req, res) => {

    const r = new Response();
    const { questions } = req.body;
    const { survey_id } = req.body;

    try {

        if (!questions || !survey_id) {
            throw ({ status: 400, message: 'No question information was provided.' });
        }

        const survey = await Survey.findByPk(survey_id);

        if (survey == null) {
            throw ({ status: 400, message: 'The provided survey does not exist' });
        }

        const newQuestions = await Question.bulkCreate(questions);
        const linkData = newQuestions.map(q => q.question_id);
        const linkResult = await survey.addQuestions(linkData);

        r.data = {
            survey: survey,
            questions: newQuestions
        }

        r.status = 201;

    } catch (e) {
        let error = e;
        if (e.errors && e.errors.length > 0 && e.errors[0].message) {
            error = {
                message: e.errors[0].message,
                status: 400
            }
        }
        r.setError(error);
    }
    return res.status(r.status).json(r);
});


module.exports = router;