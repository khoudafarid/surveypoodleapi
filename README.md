# Survey Poodle API

Survey Poodle API is built using Node.JS with Express.

## Install
Before running the application you will need to execute `npm install` to install all the requried dependencies. 

## Run
Running the project is done by executing `npm start`.

## Endpoints
See documentation for endpoints.